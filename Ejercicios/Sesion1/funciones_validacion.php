<?php

/**
 * Valida que se haya ingresado un valor
 *
 * @param mixed $valor Valor que se debe validar
 * @return bool True si se ingresó un valor False en caso contrario
 */
function requerido($valor) {

}

/**
 * Valida la longitud de una cadena, de acuerdo a la opción
 * especificada en el segundo parametro
 *
 * Ejemplos: 
 * longitud($curp, ['exacto' => 18])
 * longitud($nombre, ['min' => 5, 'max' => 100])
 *
 * @param mixed $valor Valor que se debe validar
 * @param array $opciones Arreglo para especificar opciones 'exacto', 'min', 'max' (obligatorio especificar)
 * 
 * @return True si el valor cumple con la longitud, False en caso contrario
 */
function longitud($valor, $opciones) {

}

/**
 * Valida que el valor cumpla con el formato especificado en la expresion regular
 *
 *
 * @param mixed $valor Valor que se debe validar
 * @param string $regex Expresion regular contra la que se desea validar
 * 
 * @return True si el valor cumple con la expresion regular, False en caso contrario
 */
function formato($valor, $regex='//') {

}

/**
 * Valida que el valor sea un número entero
 *
 *
 * @param mixed $valor Valor que se debe validar
 * @param array $opciones Arreglo para especificar opcionalmente 'min' y 'max'
 * 
 * 
 * @return True si el valor es un numero entero y cumple las especificaciones, False en caso contrario
 */
function numeroEntero($valor, $opciones=[]) {

}

/**
 * Valida que el valor se encuentre dentro de la lista especificada
 *
 * @param mixed $valor Valor que se debe validar
 * @param array $set Lista de valores donde se desea buscar el valor
 *
 * @return True si el valor se encuentra en la lista, False en caso contrario
 */
function enLista($valor, $set=[]) {

}

?>