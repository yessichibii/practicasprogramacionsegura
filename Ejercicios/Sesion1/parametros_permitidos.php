<?php

function parametros_permitidos($listaParametros=[]) {
	$permitidos = [];
	foreach($listaParametros as $parametros) {
		if(isset($_GET[$parametros])) {
			$permitidos[$parametros] = $_GET[$parametros];
		} else {
			$permitidos[$parametros] = NULL;
		}
	}
	return $permitidos;
}

$get_parametros = parametros_permitidos(['username', 'password']);

var_dump($get_parametros);

// http://localhost/~kevinskoglund/allowed_params.php?username=kskoglund&password=secret&logged_in=1

?>
