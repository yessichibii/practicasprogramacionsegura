<?php
require_once('funciones_validacion_end.php');

// http://localhost/seguridadphp/pruebas_validacion.php?prueba=s
$valor = $_GET['prueba'];
echo $valor;
echo "<br />";

if(requerido($valor)) {
	echo "Presente.";
} else {
	echo "No presente.";
}
echo "<br />";

if(longitud($valor, ['min' => 3, 'max' => 5])) {
	echo "Longitud válida.";
} else {
	echo "Longitud inválida.";
}
echo "<br />";

if(formato($valor, '/\d{4}/')) {
	echo "Formato válido.";
} else {
	echo "Formato inválido.";
}
echo "<br />";

if(numeroEntero($valor, ['min' => 100, 'max' => 1000])) {
	echo "Número válido.";
} else {
	echo "Número inválido.";
}
echo "<br />";

if(enLista($valor, [1,3,5,7,9])) {
	echo "Incluido en la lista.";
} else {
	echo "No incluido en la lista.";
}
echo "<br />";

?>