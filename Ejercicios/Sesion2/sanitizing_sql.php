<?php // SQL sanitization

$sanitize = false;

$nombre = isset($_GET['nombre']) ? $_GET['nombre'] : "";

if($sanitize) {
	$nombre = addslashes($nombre);
}

echo "SELECT * FROM customers WHERE name='{$nombre}';";

/**
 * Prueba con cada url en el navegador y observa los resultados cambiando el valor de sanitize
 * Ejermplos de la URL para probar los datos sanitizados
 */
// sanitizing_sql.php?nombre=Kevin
// sanitizing_sql.php?nombre=%27+OR+1=1;--
// sanitizing_sql.php?nombre=%27;+DROP+TABLE+customers;--

?>
