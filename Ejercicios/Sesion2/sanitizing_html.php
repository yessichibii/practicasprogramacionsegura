<?php // HTML/JavaScript sanitization

/**
 *Quita los comentarios a las funciones y observa como 
 *cambia el formato de las variables.
 */

$sanitize = true;

$html_string = "<div style=\"color: red; font-size: 30px;\">" . 
	"Este <strong>parrafo</strong> de texto y " .
	"<span style=\"color: green;\">HTML</span>.".
	"</div>".
	"<br />";

$javascript_string = "<script>alert('Alerta!');</script>";

if($sanitize) {
	echo "htmlspecialchar: </br>";
	echo htmlspecialchars($html_string);
	// echo "</br>htmlentities: </br>";
	// echo htmlentities($html_string);
	// echo "</br>strip_tags: </br>";
	// echo strip_tags($html_string);

	echo "htmlspecialchar: </br>";
	echo htmlspecialchars($javascript_string);
	// echo "</br>htmlentities: </br>";
	// echo htmlentities($javascript_string);
	// echo "</br>strip_tags: </br>";
	// echo strip_tags($javascript_string);
	
}
	echo $html_string;
	echo $javascript_string;


?>
