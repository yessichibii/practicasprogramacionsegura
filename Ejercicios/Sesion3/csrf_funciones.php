<?php
session_start();
require_once 'csrf_peticion_tipo_funciones.php';
require_once 'csrf_token_funciones.php';

/**
 * Asegurarse que el formulario se envia por POST
 * Manda un token para validar el formulario
 * verifica que el token sea valido
 * verifica que el token no tenga  mas de 3 minutos
 * en caso de iniciar sesion mostrar los datos mandados por el formulario:
 * nombre de usuario, tipo y token
 * guardar los datos del usuario en la sesion
 */

?>
<html>
	<head>
		<title>CSRF Inicio de sesion</title>
	</head>
	<body>
		<?php echo isset($message)?$message:""; ?><br/><br/>
		<form action="" method="post">
			<?php echo csrf_token_tag(); ?>
			<label for="nombre">Nombre de Usuario:</label><br/>
			<input type="text" name="nombre" /><br/>
			<label for="password">Contraseña: </label><br/>
			<input type="password" name="password"><br/>
			<input type="hidden" name="tipo" value="usuario"><br/>
			<input type="submit" value="Enviar datos" />
		</form>
	</body>
</html>
