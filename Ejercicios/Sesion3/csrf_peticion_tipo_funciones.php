<?php
/**
 * Las peticiones GET no deben hacer cambios,
 * solo las peticiones por POST
 */

 /**
  * @return bool regresa true si la petición se da por GET
  */
function peticion_get() {

}

/**
  * @return bool regresa true si la petición se da por POST
  */
function peticion_post() {

}

 
?>
