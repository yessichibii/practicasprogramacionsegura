<?php
//Es necesario llamar a la función session_start() antes de cargar los datos con $_SESSION

/**
 * Genera un token aleatorio para usar protección por CSRF, 
 * debe ser un valore aleatorio
 * @return string retorna el token generado
 */
function csrf_token() {
	return md5(uniqid(rand(), TRUE));
}

/**
 * Genera y almacena el token CSRF generado en la sesión del usuario,
 * guarda el token y la hora de creación
 * (requiere que la sesión este iniciada)
 * @return string retorna el token despues de guardar la información en la sesión
 */
function crear_csrf_token() {
	
}

/**
 * Borra la información del token de la sesion
 * @return bool true al destruirlo
 */
function destruir_csrf_token() {
  
}

/**
 * Función "csrf_token_tag();" que puede empearse directamente para generar
 * la etiqueta con el campo creando el token y guardadndolo en sesion.
 * @return string Devuelve la etiqueta HTML que incluye el token CSRF
 */
function csrf_token_tag() {
	
}

/**
 * Valida que el token es enviado por POST
 * y este es igual al token almacenado en la sesión
 * @return bool regresa verdadero si el token es valido
 */
function csrf_token_es_valido() {

}

/**
 * Verifica la validez del token
 */
function die_csrf_token_falla() {
	
}

/**
 * Verifica si el token ha cumplido un lapso de tiempo
 * establece un timpo válido y verifica que no ha caducado
 * @param int $tiempo_máximo tiempo que tarda en caducar dado en segundos
 * @return bool retorna verdadero si no ha caducado
 */
function csrf_token_es_reciente($tiempo_maximo) {

}

?>
