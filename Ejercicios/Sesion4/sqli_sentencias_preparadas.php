<?php 

// Conectarse a la base de datos
// $mysqli = new mysqli("localhost", "username", "password", "database");
$mysqli = new mysqli("localhost", "root", "", "seguridad");

if($mysqli->connect_errno) {
  die("Conexion Fallida: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error);
}

/**
 *  2. Prepara la sentencia pra ser ejecutada con "execute()", 
 * se mandan los parametros donde se sustituyen los datos
 * */
$sql = "SELECT usuario_id, usu_nombre, usu_correo_electronico FROM usuario WHERE usu_correo_electronico = ? AND usu_contrasenia = ?";
$stmt = $mysqli->prepare($sql);

if(!$stmt) {
	die("La Sentencia Prepara ha fallado: (" . $mysqli->errno . ") " . $mysqli->error);
}

/**
 * 3.- BIND_PARAMS enlaza los marcadores de la sentencia preparada con los parametros.
 * Tipos de parametros
 * s = string
 * i = integer
 * d = double (float)
 *  = blob (binary data)
 */

$correo = 'jpg@correo.com';
$password = 'hola123';

$bind_result = $stmt->bind_param("ss", $correo, $password);
if(!$bind_result) {
	echo "Vinculación Fallida: (" . $stmt->errno . ") " . $stmt->error;
}

/**
* 4. Execute. ejecuta la sentencia preparada con los parametros establecidos con bind_param o colocando directamente
* los parametros
*/
$execute_result = $stmt->execute();
if(!$execute_result) {
  echo "Ejecucion Fallida: (" . $stmt->errno . ") " . $stmt->error;
}

$stmt->store_result();

/**
 * 5.- Bind_result Vincula los resultados con las variables
 */
$stmt->bind_result($id, $usuario, $email);

/**
 * 6.- $stmt->fetch() obtiene los resultados en las variables establecidas
 */
while($stmt->fetch()) {
	echo 'ID: ' . $id . '<br />';
	echo 'Nombre: ' . $usuario . '<br />';
	echo 'Nombre: ' . $email . '<br />';
	echo '<br />';
}

// 7. Free results limpia la memoria
$stmt->free_result();

// 8. Close Cierra Termina la sentencia preparada
$stmt->close();

// 9. Close Cierra la conexión con la BD
$mysqli->close();

?>
