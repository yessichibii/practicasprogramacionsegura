<?php
 
/**
 * Empleando sentencias preparadas para evitar el ataque por SQL Inyection
 */

 /**
  * Información de la base de datos
  */
 $host = 'localhost';
 $puerto = '5432';
 $nombreBase = 'seguridadphp';
 $usuario = 'postgres';
 $password = 'root';

/**
 * Parametros enviados por GET
 */
$id = $_GET['id'];

/**
 * Conectarse a la base de datos, generar la query y prepara las sentencias para la consulta
 */
$dbconn = pg_connect("host=".$host." port=".$puerto." dbname=".$nombreBase." user=".$usuario." password=".$password);

/**
 * Consulta tradicional sin sentencias preparadas
 */
$query = "select usuario_id, usu_nombre, usu_primer_apellido, usu_contrasenia from usuario where usuario_id = $id";
// $result = pg_query($dbconn, $query);


/**
 * Empleando sentencias preparadas
//  */
$query = "select usuario_id, usu_nombre, usu_primer_apellido, usu_contrasenia from usuario where usuario_id = $1";
$result = pg_prepare($dbconn, "miquery", $query);
$result = pg_execute($dbconn, "miquery", array($id));

/**
* Mostrar los resultados
*/

echo "<pre>";
print_r(pg_fetch_all($result));
echo "</pre>";

 /**
  * Observa que sucede si en la URL introduces los siguientes ejemplos de ataque.
  * Observa la información que se muestra y los cambios presentes en la base de datos
  * compara con lo observado sin emplear las sentencias preparadas
  */
//?id=1 UNION ALL SELECT NULL,version(),NULL, NULL LIMIT 1 OFFSET 1--
//?id=1; UPDATE usuario SET usu_contrasenia=chr(114)||chr(111)||chr(111)||chr(116)--
//?id=1%20UNION%20ALL%20SELECT%20NULL,user,NULL,NULL--
//?id=1%20UNION%20ALL%20SELECT%20NULL,current_database(),NULL,NULL--
//?id=1;%20CREATE%20TABLE%20file_store(id%20serial,%20data%20text)--