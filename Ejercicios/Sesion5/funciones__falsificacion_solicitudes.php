<?php

// Puede emplearse con "request_is_post() para bloquear la publicación de formularios fuera del sitio
/**
 * @return bool la petición se realiza desde el mismo dominio
 */
function solicitud_del_mismo_dominio() {
	if(!isset($_SERVER['HTTP_REFERER'])) {
		//si no existe el remitente no puede ser el mismo dominio
		return false;
	} else {
		$referencia_host = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
		$servidor_host = $_SERVER['HTTP_HOST'];

		echo 'Solicitud de: ' . $referencia_host . "<br />";
		echo 'Solicitud a: ' . $servidor_host . "<br />";

		return ($referencia_host == $servidor_host) ? true : false;
	}
}
/**
 * Mostrar los resultados
 */
if(solicitud_del_mismo_dominio()) {
	echo 'El mismo dominio. La solicitud POST es permitida. <br/>';
} else {
	echo 'Diferente dominio, La solicitud POST esta prohibida.<br/>';
}
echo '<br/>';
echo '<a href="">Link del mismo dominio</a><br/>';

?>
