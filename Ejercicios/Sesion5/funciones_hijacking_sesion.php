<?php

// Ajustes para el archivo de configuración php.ini
// session.cookie_lifetime = 0
// session.cookie_secure = 1
// session.cookie_httponly = 1
// session.use_only_cookies = 1
// session.entropy_file = "/dev/urandom"

session_start();

// Funcion para finalizar la sesión
function finalizar_session() {
	// Usar ambas para evitar problemas de compatibilidad con los navegadores
	// y todas las versiones de PHP
	session_unset();
	session_destroy();
}
 
/**
 * Verifica la IP de la solicitud
 * @return bool true si es valida
 */
function verificar_ip_session() {
	// Retorna falso si no tiene valor
	if(!isset($_SESSION['ip']) || !isset($_SERVER['REMOTE_ADDR'])) {
		return false;
	}
	if($_SESSION['ip'] === $_SERVER['REMOTE_ADDR']) {
		return true;
	} else {
		return false;
	}
}

/**
 * Verifica el agente de la sesion
 * @return bool true si es valido
 */
function verificar_agente_session() {
	// Retorna falso si no tiene valor
	if(!isset($_SESSION['user_agent']) || !isset($_SERVER['HTTP_USER_AGENT'])) {
		return false;
	}
	if($_SESSION['user_agent'] === $_SERVER['HTTP_USER_AGENT']) {
		return true;
	} else {
		return false;
	}
}

/**
 *  Verifica el tiempo desde el ultimo inicio de sesion
 * @return bool true si se encuentra dentro del lapso establecido
 */
function verificar_ultimo_inicio_sesion() {
	$max_elapsed = 60 * 60 * 24; // 1 dia
	// Retorna falso si no tiene valor
	if(!isset($_SESSION['last_login'])) {
		return false;
	}
	if(($_SESSION['last_login'] + $max_elapsed) >= time()) {
		return true;
	} else {
		return false;
	}
}

/**
 *  Muestra si la sesion es valida
 * @return true si es valida
 */
function sesion_valida() {
	$check_ip = true;
	$check_user_agent = true;
	$check_last_login = true;

	if($check_ip && !verificar_ip_session()) {
		return false;
	}
	if($check_user_agent && !verificar_agente_session()) {
		return false;
	}
	if($check_last_login && !verificar_ultimo_inicio_sesion()) {
		return false;
	}
	return true;
}

/**
 * Función para redireccionar a la pagina establecida y destruir la sesion
 * en caso de no tener una sesion valida
 */
function confirma_sesion_valida() {
	if(!sesion_valida()) {
		finalizar_session();
		header("Location: login.php");
		exit;
	}
}


/**
 * Verifica si el usuario ya ha iniciado sesion
 * @return bool
 */
function verifica_inicio_sesion() {
	return (isset($_SESSION['logged_in']) && $_SESSION['logged_in']);
}

/**
 * Si no ha iniciado sesión redirecciona al usuario a una pagina establecida y destruye la sesion
 */
function confirma_usuario_inicia_sesion() {
	if(!verifica_inicio_sesion()) {
		finalizar_session();
		header("Location: login.php");
		exit;
	}
}


/**
 * Acciones a realizar al completar un inicio de sesion exitoso
 */
function despues_sesion_iniciada() {
	// Regenera el ID de la sesión invalidando los parametros anteriores
	// Esto evita la fijación o secuestro de la sesión
	session_regenerate_id();
	
	$_SESSION['logged_in'] = true;

	// Guarda los nuevos valores de la sesión, incluso sin validaciones
	$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
	$_SESSION['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
	$_SESSION['last_login'] = time();
	
}

/**
 * Acciones a realizar despues de cerrar la sesion
 */
function despues_sesion_finalizada() {
	$_SESSION['logged_in'] = false;
	finalizar_session();
}

/**
 * Acciones a realizar antes de dar acceso a los usuarios
 * Paginas con acceso restringido
 */
function antes_acceso() {
	confirma_usuario_inicia_sesion();
	confirma_sesion_valida();
}



if(isset($_GET['action'])) {
	if($_GET['action'] == "login") {
		despues_sesion_iniciada();
	}
	if($_GET['action'] == "logout") {
		despues_sesion_finalizada();
	}
} 

echo "Session ID: " . session_id() . "<br />";
echo "Sesion iniciada: " . (verifica_inicio_sesion() ? 'true' : 'false') . "<br />";
echo "Sesion valida: " . (sesion_valida() ? 'true' : 'false') . "<br />";
echo "<br />";
echo "--- SESSION ---<br />";
var_dump($_SESSION);
echo "--------------------<br />";
echo "<br />";

echo "<a href=\"?action=new_page\">Simulate a new page request</a><br />";
echo "<a href=\"?action=login\">Simulate a log in</a><br />";
echo "<a href=\"?action=logout\">Simulate a log out</a>";

?>
