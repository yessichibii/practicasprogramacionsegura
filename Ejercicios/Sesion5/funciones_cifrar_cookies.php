<?php
// Funciones de cifrado de cookies
// Requiere el uso de "mcrypt". http://php.net/manual/en/book.mcrypt.php


/**
 * Función de cifrado
 * @return vector de inicialización y cadena cifrada
 */
function cifrando_cadena($salt, $cadena) {
	// Configuracion (El cifrado debe coincidir con el decifrado)
	$tipo_cifrado = MCRYPT_RIJNDAEL_256;
	$modo_cifrado = MCRYPT_MODE_CBC;
	
	/** 
	 * Emplea un vector de inicialización para agragar seguridad
	 * */
	$iv_tamanio = mcrypt_get_iv_size($tipo_cifrado, $modo_cifrado);
	$iv =  mcrypt_create_iv($iv_tamanio, MCRYPT_RAND);

	$cadena_cifrada = mcrypt_encrypt($tipo_cifrado, $salt, $cadena, $modo_cifrado, $iv);
	
	// Retorna el vector de inicialización y la cadema cifrada
	// Se requiere del vector de inicialización para descifrar
	return $iv . $cadena_cifrada;
}

/**
 * Descifrar cadena
 * @return cadena 
 */
function decifrando_cadena($salt, $iv_cadena) {
	// Configuracion (Deben ser los mismos valores que al cifrar)
	$tipo_cifrado = MCRYPT_RIJNDAEL_256;
	$modo_cifrado = MCRYPT_MODE_CBC;
	
	// Recuperar el vector de inicialización de la cadena cifrada.
	// se encuentra concatenado al inicio de la cadena
	$iv_tamanio = mcrypt_get_iv_size($tipo_cifrado, $modo_cifrado);
	$iv = substr($iv_cadena, 0, $iv_tamanio);
	$cadena_cifrada = substr($iv_cadena, $iv_tamanio);
	
	$cadena = mcrypt_decrypt($tipo_cifrado, $salt, $cadena_cifrada, $modo_cifrado, $iv);
	return $cadena;
}

// Codificar la cadena cifrada para asegurarse de que los caracteres sean salvables
function codificar_cadena_cifrada($salt, $cadena) {
	return base64_encode(cifrando_cadena($salt, $cadena));
}

// Decifara la cadena y decodificarla
function decifrar_cadana_codificada($salt, $cadena) {
	return decifrando_cadena($salt, base64_decode($cadena));
}


$my_salt = 'Cadena_aleatoria-hY5K92AzVnMYyT7';

$cadena = 'Ejemplo de texto en claro';
echo "<br/>Texto original: " . $cadena . "<br/>";
echo "<br/>--------------------------------------<br/>";

$cadena_cifrada = cifrando_cadena($my_salt, $cadena);
echo "<br/>Texto cifrado: ". $cadena_cifrada ."<br/>";
echo "<br/>--------------------------------------<br/>";

$cadena_decifrada = decifrando_cadena($my_salt, $cadena_cifrada);
echo "<br/>Texto decifrado: ". $cadena_decifrada . "<br/>";
echo "<br/>--------------------------------------<br/>";

$cadena_codificada = codificar_cadena_cifrada($my_salt, $cadena);
echo "<br/>Texto cifrado y codificado: ". $cadena_codificada . "<br/>";
echo "<br/>--------------------------------------<br/>";

$cadena_inicial = decifrar_cadana_codificada($my_salt, $cadena_codificada);
echo "<br/>Texto decodificado y decifrado: ". $cadena_inicial . "<br/>";
echo "<br/>--------------------------------------<br/>";
?>
