<?php

// Ejemplo del uso de "cookie" firmado

function firmar_cadena($cadena) {
	// El uso de "$salt" hace que sea más dificil encontrar la manera en que se genera la verificación $verificar
	// ¡Precuación! Al cambiar "$salt" se invalidan todas las cadenas que han sido firmadas.
	$salt = "Simple salt";
	$verificacion = sha1($cadena.$salt); // Se puede emplear cualquier algoritmo hash, en este casho se emplea sha1
	// retorna la cadena con el valor de verificacion al final.
	return $cadena.'--'.$verificacion;
}

function cadena_firmada_es_valida($cadena_firmada) {
	$array = explode('--', $cadena_firmada);
	if(count($array) != 2) {
		// La cadena no esta bien formada o no es valida
		return false;
	}
	 
	// Al firmar la misma cadena con e mismo salt y hash, 
	// deberia dar el mismo codigo de verificación.
	$nueva_cadena_firmada = firmar_cadena($array[0]);
	if($nueva_cadena_firmada == $cadena_firmada) {
		return true;
	} else {
		return false;
	}
}

$cadena = "Cadena de prueba.";
echo "Cadena original: " . $cadena . "<br/>";
echo "<br/>----------------------------------------------<br/>";

$cadena_firmada = firmar_cadena($cadena);
echo "Cadena firmada: " . $cadena_firmada . "<br/>";
echo "<br/>";

$valida = cadena_firmada_es_valida($cadena_firmada);
echo "¿Cadena valida? " . ($valida ? 'true' : 'false') . "<br/>";
echo "<br/>----------------------------------------------<br/>";

$cadena_sin_salt = sha1($cadena);
echo "Cadena con hash sin salt: Cadena de prueba.--" . $cadena_sin_salt . "<br />";
echo "<br/>";

$valida = cadena_firmada_es_valida($cadena_firmada);
echo "¿Cadena valida? " . ($valida ? 'true' : 'false') . "<br/>";
echo "<br/>----------------------------------------------<br/>";

$cadena_manipulada = "Cadena de prueba.--521d61df5d4c239a1f5a191ff3803826b60587a9";
echo "Cadena manipulada: " . $cadena_manipulada . "<br />";
echo "<br/>";

$valida = cadena_firmada_es_valida($cadena_manipulada);
echo "¿Cadena valida? " . ($valida ? 'true' : 'false') . "<br/>";
echo "<br/>--------------------------------------------------<br/>";

?>
