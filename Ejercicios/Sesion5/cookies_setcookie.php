<?php
// Seguridad empleando cookies

// setcookie(name, value, expire, path, domain, secure, httponly)
//
// Cuando se pasan parametros null emplea los valroes por defaults
// Defaults:
//   expire = 0 (expires when browser closes)
//   path = (current directory)
//   domain = (current domain)
//   secure = false
//   httponly = false (note: not respected by all browsers)

$path = '/';
$domain = 'localhost';
$secure = isset($_SERVER['HTTPS']);
$httponly = true; // JavaScript can't access cookie

$name = 'username';
$value = 'kskoglund';   
$expire = time() + 10 ; // Durante 10 segundos
if(isset($_COOKIE['username'])){
    echo 'username: '.($_COOKIE['username']);
    echo  "</br>";
}else{
    echo "Cookie 'username' caducada o inexistente</br>";
    setcookie($name, $value, $expire, $path, $domain, $secure, $httponly);
}

if(isset($_COOKIE['contador'])){
    $value2 = $_COOKIE['contador']+1;
    echo 'username: '.($_COOKIE['contador']);
    echo  "</br>";  
}else{
    $value2 = 1;
    echo "Cookie 'contador' caducada o inexistente</br>";
}
$name2 = 'contador';
$expire2 = time() + 20 ; // Durante 20 segundos
setcookie($name2, $value2, $expire2, $path, $domain, $secure, $httponly);
    
?>
