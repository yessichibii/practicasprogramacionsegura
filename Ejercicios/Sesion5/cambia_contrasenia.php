<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Cambiar contraseña</title>
  </head>
  <body>

<?php

if(isset($_POST['submit'])) {
	
	$contrasenia = $_POST['contrasenia'];
	echo "Contraseña: " . $contrasenia . "<br />";
  
	// Ejemplo de cifrado
	$contrasenia_cifrada = password_hash($contrasenia, PASSWORD_BCRYPT);
	echo "Contraseña cifrada: " . $contrasenia_cifrada . "<br />";
	echo "<br />";
	
	// Verification 
	$contrasenia_incorrecta = "valor de prueba";
	$verificar = password_verify($contrasenia_incorrecta, $contrasenia_cifrada);
	echo "Prueba 1: " . ($verificar ? 'correct' : 'wrong') . "<br />";

	$verificar = password_verify($contrasenia, $contrasenia_cifrada);
	echo "Prueba 2: " . ($verificar ? 'correct' : 'wrong') . "<br />";
	echo "<br />";
	echo "<hr />";
	
}

?>

    <p>Cambia tu contraseña.</p>
    
    <form action="cambia_contrasenia.php" method="POST" accept-charset="utf-8">
			Contraseña: <input type="contrasenia" name="contrasenia" value="" /><br />
			<br />
      <input type="submit" name="submit" value="Cambia contrasenia" />
    </form>

  </body>
</html>
