CREATE TABLE usuario (
       usuario_id                SERIAL,
       usu_nombre                VARCHAR(50) NOT NULL,
       usu_primer_apellido       VARCHAR(50) NOT NULL,
       usu_segundo_apellido      VARCHAR(50) NULL,
       usu_correo_electronico    VARCHAR(100) NOT NULL,
       usu_contrasenia           VARCHAR(60) NOT NULL,
       usu_estatus               CHAR(1) NOT NULL,
       PRIMARY KEY (usuario_id)
);

insert into usuario VALUES (1, 'Jorge', 'Pérez', 'García', 'jpg@correo.com', 'hola123', 'A');

