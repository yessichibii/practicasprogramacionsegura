<?php
    $tipo = array("Usuario" => 1,"Administrador" => 2);

?>
<!doctype html>
<html>
<head>
    <title>Formulario</title>
</head>
<body>
    <h1>Formulario Práctica 2</h1>
    <hr>
    <form action="leerFormulario.php" method="GET">
        <fieldset>
            <legend>Datos generales</legend>
            <div>
                <label for="usuario">Nombre:</label>
                <input id="usuario" type="text" name="usuario">
            </div><br>
            <div>
                <label for="cuenta">Numero de Cuenta:</label>
                <input id="cuenta" type="text" name="cuenta">
            </div><br>
            <div>
                <label for="email">Direccion email:</label>
                <input id="email" type="text" name="email">
            </div><br>
            <div>Genero: <br>
                <input type="radio" id="masculino" name="genero" value="masculino">
                <label for="masculino">Masculino</label><br>
                <input type="radio" id="femenino" name="genero" value="femenino">
                <label for="femenino">Femenino</label><br>
            </div><br>
            
        </fieldset>
        <a href="formulario.php?tipo=<?php echo $tipo; ?>">Agregar tipo</a>
        <div>
            <input type="submit" name="submit" value="Enviar informacion"> 
        </div>
    </form>
</body>
</html>