<?php
    require_once('funciones.php');

?>
<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Inicio Sesión</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Estilos Personalizados -->
    <link href="css/style.css" rel="stylesheet">
  </head>

  <body class="text-center">
    <div class="formulario">
      <form class="form-signin" action="leerFormulario.php" >
      <h1>Formulario Práctica 3</h1>
        <img class="mb-4" src="img/user.svg" alt="" width="120" height="120">
        <h2 class="h3 mb-3 font-weight-normal">Iniciar Sesión</h2>
        <label for="usuario" class="sr-only">Nombre de usuario:</label>
        <input type="text" id="usuario" name="usuario" class="form-control" placeholder="Nombre de usuario" required autofocus>
        <label for="password" class="sr-only">Contraseña:</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Contraseña" required>
        <input id="tipo" type="hidden" name="tipo" value="usuario">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Enviar</button>
        <p class="mt-5 mb-3 text-muted">&copy; 2018-2019</p>
      </form>
    </div>
  </body>
</html>
