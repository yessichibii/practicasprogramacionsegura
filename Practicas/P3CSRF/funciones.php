<?php

/**
*Valida que los parametros que se envian por método POST son los que se esperan
* @param array $listaParametros lista de parametros permitidos
* @return array $permitidos lista de valores de los parametros permitidos
*/
function parametros_permitidos($listaParametros=[]) {

}


/**
 * Valida que se haya ingresado un valor
 * @param mixed $valor Valor que se debe validar
 * @return bool True si se ingresó un valor False en caso contrario
 */
function requerido($valor) {

}



/**
 * Genera y almacena el token CSRF en la sesión del usuario,
 * es necesario que inicie la sesion para poder guardar los datos
 * @return string Retorna el token generado
 */
function crear_csrf_token() {
	borrar_datos_sesion();

}



/**
 * Valida si el token es enviado por método POST,
 * comprueba si el token que fue enviado es el mismo al que se tienealmacenado
 * @return bool True si el token es enviado por metodo post y el token almacenado es igual al que es enviado.
 */
function csrf_token_es_valido() {

}


/**
 * Verifica si el token ha cumplido un lapso de tiempo
 * en caso de no cumplir con el periodo de tiempo borra la información del token.
 * @return bool True si el tiempo no ha caducado
 */
function csrf_token_es_reciente() {
    // $tiempo_maximo = ? ; // El tiempo se mide en segundos
    // if (condition) {
    //     # code...
    // } else {
    //     borrar_csrf_token();
    //     # code...
    // }
}

/**
 * Borra el token generado
 * @return bool True si se borran los datos
 */
function borrar_csrf_token() {

}
 
/**
 * Borra la informacion que se encuentre en la sesion
 * @return bool True si se borran los datos
 */
function borrar_datos_sesion() {

}
 

?>